#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git libpng-dev libmcrypt-dev libxml2-dev -yqq

# Here you can install any extension that you need
docker-php-ext-configure mcrypt
docker-php-ext-install pdo_mysql gd mcrypt zip soap
<?php

/**
 * @package    Gpaddis_MagentoModule
 * @author     Gianpiero Addis <gianpiero.addis@gmail.com>
 *
 * PHPUnit Class Annotations
 * @group Gpaddis_MagentoModule
 */
class Gpaddis_MagentoModule_Test_Model_Foo extends EcomDev_PHPUnit_Test_Case
{
    /**
     * @test
     */
    public function exampleTest()
    {
        $this->assertTrue(true);
    }

    /**
     * @test
     * @loadFixture ~Gpaddis_MagentoModule/defaultData
     */
    public function it_loads_the_configuration_from_the_database()
    {
        $configuration = Mage::getStoreConfigFlag('magentomodule/config/enable');
        $this->assertTrue($configuration);
    }
}

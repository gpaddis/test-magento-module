# gpaddis/magentomodule

[![pipeline status](https://gitlab.com/gpaddis/test-magento-module/badges/master/pipeline.svg)](https://gitlab.com/gpaddis/test-magento-module/commits/master)

Just a sample project to integrate **Ecomdev_PHPUnit** with GitLab CI.

Checkout the test script: https://github.com/AOEpeople/MageTestStand